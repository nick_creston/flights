import { format} from 'date-fns'
import ru from 'date-fns/locale/ru'

function gethoursMinutes(sec) {
  let h = sec/3600 ^ 0 ;
  let m = (sec-h*3600)/60 ^ 0 ;

  return (h<10?"0"+h:h)+" ч. "+(m<10?"0"+m:m)+" мин"
}
function prepareFlightsData(flights) {
  let flytime = 0
  let waittime = 0
  let dict = {}
  flights.forEach((item) =>{
    flytime += parseInt(item.flytime);
    waittime +=  parseInt(item.waittime);
    dict[item.from.airport.code_en] = {
      port: item.from.airport.code_en,
      waittime: (item.waittime != 0) ?  gethoursMinutes(item.waittime) : 0,
      date: format(new Date(item.from.time), 'HH:mm', {locale: ru})
    }
    dict[item.to.airport.code_en] = {
      port:item.to.airport.code_en,
      waittime: (item.waittime != 0) ? gethoursMinutes(item.waittime) : 0,
      date: format(new Date(item.to.time), 'HH:mm', {locale: ru})
    }
  })
  let result = {
    fulltime: flytime+waittime,
    fulltimeText: gethoursMinutes(flytime+waittime),
    transfer: dict
  }
  return result;
}

export function prepareData (data){
  data.map((item, index)=> {  
    let flyghtsData = prepareFlightsData(item.flights);
    item['flyghtsData'] = flyghtsData
    item.baggage = item.baggage === '' ? 'НЕТ': item.baggage
  })
}

export function filterData(data, filters) {
  let result = data;
  if(filters.withBag === true) {
    result = result.filter(item => item.baggage !== '' && item.baggage !== 'НЕТ')
  }
  if(filters.directFlight === true) {
    result = result.filter(item => item.flights.length === 1)
  }

  return result;
}
