import React from "react";
import { render } from 'react-dom';
import { Provider } from "react-redux";
import store from "./store";
// this line is new
// we now have some nice styles on our react app
import "styles/index.scss";
// import App from './App'
import AppContainer from "./containers/AppContainer"

// const ProviderApp = () =>( <Provider store={store}><App/></Provider>)

// const renderApp = Component => {
//   render(<Component/>, document.getElementById("app"))
// }
// ;

// renderApp(App);

// if (module.hot) {
//   module.hot.accept(() => renderApp(ProviderApp));
// }


render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById("app")
);
