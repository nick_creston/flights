import { connect } from "react-redux"
import CardList from  "../components/CardList.jsx"

const mapStateToProps = state => {
  return { flights: state.flights }
};

export default connect(mapStateToProps)(CardList)

