import { connect } from "react-redux";

import { loadFlights, sortPriceAsc, sortPriceDesc, sortTimeAsc, sortTimeDesc, reset } from "../actions"
import App from "../components/App";

const mapStateToProps = state => {
  return { flights: state.flights, loading: state.loading, error_msg: state.error_msg };
};


export default connect(mapStateToProps, {loadFlights, sortPriceAsc, sortPriceDesc, sortTimeAsc, sortTimeDesc, reset})(App)
