import React from "react";

const NoData = () => (<div className="alert alert-info mt-5" role="alert">Нет данных</div>)

export default NoData

