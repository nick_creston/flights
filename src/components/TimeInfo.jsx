import React from "react";

import PropTypes from "prop-types";

const TimeInfo = ({time, text}) => (<div className="time-info-container">
  <div className="time-info-time">{time}</div>
  <div className="time-info-text">{text.toUpperCase()}</div>
</div>
)

TimeInfo.propTypes = {
  time: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
}

export default TimeInfo

