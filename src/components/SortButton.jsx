import React from "react";

import PropTypes from "prop-types";


const SortButton = ({state, title, onClick}) => (<div className="sort-btn ml-2" onClick={onClick}>{title}
<div className="triangle ml-2">
  {state !== null && <div className={ ((state) ? 'up': 'down') +'-arrow' }></div>}
</div>
</div>)

SortButton.propTypes = {
    state: PropTypes.oneOf([null, true, false]),
    title: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired
}
export default SortButton
