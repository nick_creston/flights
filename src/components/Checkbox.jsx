import React from "react";

import PropTypes from "prop-types";

const Checkbox = ({label, id, checked, onChange}) => {
return (
  <div className="form-check">
    <input id={id} type="checkbox" className="form-check-input" checked={checked} onChange={onChange}/>
    <label htmlFor={id}>{label}</label>
  </div>
)
}

Checkbox.propTypes = {
  label: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onChange: PropTypes.func
}
export default Checkbox;

