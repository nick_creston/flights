import React from "react";
import PropTypes from 'prop-types'
import Step from './Step.jsx'


const Segments = ({data}) => {
  const {fulltimeText, transfer} = data;
  let transferLen = Object.keys(transfer).length

  return (
    <div className="segments-container">
      <div className="stepwizard">
          <div className="stepwizard-row">
            {Object.keys(transfer).map((key, index) => {
              let tipText= ""
              if(index === 0) {
                tipText = `Вылет ${transfer[key].date} (местное время)`
              }
              else if(index === (transferLen - 1)) {
                tipText = `Прилет ${transfer[key].date} (местное время)`
              }
              else {
                tipText = `Ожидание ${transfer[key].waittime}`
              }
            return (<Step key={key} tip={tipText} text={transfer[key].port}/>)
            })}
          </div>
          <div className="en_route">В пути {fulltimeText}</div>
      </div>
    </div>
  )
}

Segments.propTypes = {
  data: PropTypes.object
}

export default Segments
