import React from "react";

import Price from './Price.jsx'
import Baggage from './Baggage.jsx'
import FlightSegments from './FlightSegments.jsx'
import PropTypes from 'prop-types'


class Card extends React.Component {
  render() {
    const {data} = this.props
    return (
      <div className="card">
          <Price amount={Math.floor(data.price)} currency={data.currency}/>
          <FlightSegments flights={data.flights} flightsData={data.flyghtsData} />
          <Baggage text={data.baggage} />
      </div>
    )
  }
}

Card.propTypes = {
  data: PropTypes.object.isRequired
}

export default Card
