import React from "react"
import Card from "./Card.jsx"

 const CardList = ({flights}) => {
  return(
    <React.Fragment>
      {flights.map((item, index) => <Card key={index} data={item}/>)}
    </React.Fragment>
  )
}

export default CardList
