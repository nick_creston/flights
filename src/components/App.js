import React from "react";

import Input from "./Input.jsx";
import Checkbox from "./Checkbox.jsx"
import Button from "./Button.jsx"
import CardListContaner from "../containers/CardListContainer"
import SortButton from "./SortButton.jsx"
import NoData from "./NoData.jsx"
import Preloader from "./Preloader.jsx";
import Error from "./Error.jsx"

// import { connect } from "react-redux";

// import { loadFlights, sortPriceAsc, sortPriceDesc, sortTimeAsc, sortTimeDesc, reset, startLoading } from "./actions"

// const mapStateToProps = state => {
//   return { flights: state.flights, loading: state.loading, error_msg: state.error_msg };
// };



class App extends React.Component {
  state = {
    withBag: false,
    directFlight: false,
    sortPrice: null,
    sortTime: null,
    loading: false,
  }
  
  handleSort() {
    const {sortPriceAsc, sortPriceDesc, sortTimeAsc, sortTimeDesc} = this.props
    if(this.state.sortPrice !== null) {
      if(this.state.sortPrice) {
        sortPriceAsc();
      }
      else { 
        sortPriceDesc();
      }
    }
    if(this.state.sortTime !== null) {
      if(this.state.sortTime) {
        sortTimeAsc()
      }
      else { 
        sortTimeDesc()
      }
    }
  }
  sortPrice() {
    this.setState({sortTime: null, sortPrice: !this.state.sortPrice}, ()=> this.handleSort())
  }

  sortTime() {
    this.setState({sortPrice: null, sortTime: !this.state.sortTime}, ()=> this.handleSort())
  }

  submit() {
    const {loadFlights, reset} = this.props;
    reset()
    for(let i = 1; i <= 3; i++) {
      setTimeout(() => {
        loadFlights(i-1,{withBag: this.state.withBag, directFlight: this.state.directFlight})
      }, (i*2) * 1000);
    }
  }

  render() {
    return(
      <React.Fragment>
        <div className="form-container">
          <form>
            <div className="row">
              <div className="col-md-5 mb-3">
                <Input label="Откуда" type="text" id="from" defaultValue="САНКТ-ПЕТЕРБУРГ" />
              </div>
              <div className="col-md-5 mb-3">
                <Input label="Куда" type="text" id="to" defaultValue="МОСКВА"/>
              </div>
              <div className="col-md-2 mb-3 d-flex align-items-end">
                <Button type="primary" text="Найти" onClick={() => this.submit()}/>
                <Preloader show={this.props.loading}/>
              </div>
            </div>
            <div className="row">
              <div className="col-md-5 mb-3">
               <Checkbox label="Показать с багажом" id="without-bag" checked={this.state.withBag} onChange={()=> this.setState({withBag: !this.state.withBag})}/>
              </div>
              <div className="col-md-5 mb-3">
               <Checkbox label="Показать прямые рейсы" id="direct-flight" checked={this.state.directFlight} onChange={()=> this.setState({directFlight: !this.state.directFlight})}/>
              </div>
            </div>
          </form>
        </div>

        {this.props.error_msg && <Error msg={this.props.error_msg}/>}

        {this.props.flights.length !== 0 ?
        <React.Fragment>
        <div className="mt-5">
          Сортировать:
          <SortButton state={this.state.sortPrice} title="По цене" onClick={()=> this.sortPrice()}/> 
          <SortButton state={this.state.sortTime} title="По времени в пути" onClick={()=> this.sortTime()}/>          
        </div>
        
        <div className="mt-3">
           <CardListContaner /> 
        </div>
        </React.Fragment>

: <NoData />}
      </React.Fragment>
    )
  }
}


export default App
