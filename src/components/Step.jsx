import React from "react";

import ReactTooltip from 'react-tooltip'
import PropTypes from 'prop-types'

const Step = ({tip, text}) => (<div className="stepwizard-step">
  <span data-tip={tip} className="btn-circle"></span>
  <ReactTooltip place="top" type="info" effect="solid"/>
  <p>{text}</p>
</div>
)

Step.propTypes = {
    tip: PropTypes.string.isRequired,
    text: PropTypes.string
}

export default Step;
