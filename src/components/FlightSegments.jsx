import React from "react";
import { format} from 'date-fns'
import ru from 'date-fns/locale/ru'
import PropTypes from 'prop-types'
import TimeInfo from './TimeInfo.jsx'
import Segments from './Segments.jsx'

class FlightSegments extends React.Component {
    state = {
      arrivalTime: '',
      arrivalText: '',
      departureTime: '',
      departureText: '',
    }
    getArrivalTime(from) {
      let date = new Date(from.time);
      let localeDate = format(date, 'DD MMM, dd', {locale: ru})
      this.setState({
          arrivalTime: format(date, 'HH:mm', {locale: ru}), 
          arrivalText: `${from.city.name} ${from.airport.name} ${localeDate}`
        })
    }
  
    getDepartureTime(to) {
      let date = new Date(to.time);
      let localeDate = format(date, 'DD MMM, dd', {locale: ru})
      this.setState({
          departureTime: format(date, 'HH:mm', {locale: ru}), 
          departureText: `${to.city.name} ${to.airport.name} ${localeDate}`
        })
    }
    
    componentDidMount() {
      let flights = this.props.flights;
      let countSegment = flights.length;;
     
      this.getArrivalTime(flights[0].from)
      if(countSegment == 1) {
        this.getDepartureTime(flights[0].to)
      }
      else {
        this.getDepartureTime(flights[countSegment-1].to)
      }
    }
  
    render() {
      return (
        <div className="segments-container">
          <TimeInfo time={this.state.arrivalTime} text={this.state.arrivalText} />
          <Segments data={this.props.flightsData}/>
          <TimeInfo time={this.state.departureTime} text={this.state.departureText} />
        </div>
      )
    }
  } 

  FlightSegments.propTypes = {
    flights: PropTypes.array,
    flightsData: PropTypes.object
  }

  export default FlightSegments

