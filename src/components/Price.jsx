import React from "react";

import PropTypes from "prop-types";

const Price = ({amount, currency}) => {
  return (
    <span className="badge badge-pill badge-primary font18 p-3 price">{amount} <span className="currency">{currency}</span></span>
  )
}

Price.propTypes = {
  amount: PropTypes.number.isRequired,
  currency: PropTypes.string.isRequired
}
export default Price

