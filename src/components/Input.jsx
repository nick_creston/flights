import React from "react";

import PropTypes from "prop-types";

const Input = ({label, type, id, defaultValue}) => {
    return (
        <React.Fragment>
            <label htmlFor={id}>{label}</label>
            <input id={id} type={type} className="form-control" defaultValue={defaultValue || ''}/>
        </React.Fragment>
    )
}

Input.propTypes = {
    label: PropTypes.string.isRequired,
    id: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    defaultValue: PropTypes.string
}
export default Input;
