import React from 'react'
import PropTypes from 'prop-types'

const Baggage = ({text}) => (<div className="baggage">{text}</div>)

Baggage.propTypes = {
  text: PropTypes.string.isRequired
}
export default Baggage

