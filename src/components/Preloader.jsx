import React from "react";

const Preloader = ({show}) => (<div className="ml-2">{show &&<img src="/img/preloader.gif"/>}</div>)

export default Preloader
