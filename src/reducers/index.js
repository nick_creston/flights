
const initialState = {
  flights: [],
  loading: false,
  error_msg: ''
}
function rootReducer(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_FLIGHTS':
      return Object.assign({}, state, {
        flights: state.flights.concat(action.payload),
      })
    case 'SORT_PRICE_ASC':
      return Object.assign({}, state, {
        flights: state.flights.slice().sort((a, b) => a.price - b.price)
      })
    case 'SORT_PRICE_DESC':
      return Object.assign({}, state, {
        flights: state.flights.slice().sort((a, b) => b.price - a.price)
      })
    case 'SORT_TIME_ASC':
      return Object.assign({}, state, {
        flights: state.flights.slice().sort((a, b) => a.flyghtsData.fulltime - b.flyghtsData.fulltime)
      })
    case 'SORT_TIME_DESC':
      return Object.assign({}, state, {
        flights: state.flights.slice().sort((a, b) => b.flyghtsData.fulltime - a.flyghtsData.fulltime)
      })
    case 'RESET': 
      return {
        ...state,
        flights: initialState.flights
      }
    case 'START_LOADING':
      return {
        ...state,
        loading: true
      }
    case 'END_LOADING': 
      return {
        ...state,
        loading: false
      }
    case 'ERROR':
        return {
          ...state,
          error_msg: action.payload
        }
    default:
      return state
  }
};
export default rootReducer;
