import {prepareData, filterData} from "../utils"
import axios from "axios";


export const sortPriceAsc = () => {
  return {type: 'SORT_PRICE_ASC'}
}

export const sortPriceDesc = () => {
  return {type: 'SORT_PRICE_DESC'}
}

export const sortTimeAsc = () => {
  return {type: 'SORT_TIME_ASC'}
}

export const sortTimeDesc = () => {
  return {type: 'SORT_TIME_DESC'}
}

export const reset = () => {
  return {type: 'RESET'}
}

export const startLoading = () => {return {type: 'START_LOADING'}}


export function loadFlights(index, filters) {
  return function(dispatch) {
      dispatch({type: 'START_LOADING'})
      return axios.get('./data/LED-MOW-'+index+'.json')
        .then( (response) => {
            dispatch({ type: "LOAD_FLIGHTS", payload: showData(response.data, filters) })
            dispatch({type: 'END_LOADING'})
        })
        .catch(function (error) {
          dispatch({ type: "ERROR", payload: `Возникла ошибка ${error.response.status}`})
          dispatch({type: 'END_LOADING'})
        })
    }
        
}


function showData(data, filters) {
  prepareData(data.results)
  return filterData(data.results, filters)
}

