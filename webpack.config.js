const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const webpack = require('webpack');


module.exports =  {
  context: path.resolve(__dirname, "src"),
  entry: path.join(__dirname,'src','index.js'),
  output: {
    path: path.join(__dirname,'dist'),
    filename: 'index.bundle.js'
  },
  watchOptions: {
    poll: true
  },
  mode: process.env.NODE_ENV || 'development',
  resolve: {
    modules: [path.resolve(__dirname,'src'),'node_modules']
  },
  devServer: {
    contentBase: path.join(__dirname,'src'),
    hot: true
  },

  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.(sa|sc|c)ss$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
          'sass-loader',
        ],
      },
    ]
  },
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "[name].css",
      chunkFilename: "[id].css"
    }),
    new HtmlWebpackPlugin({
      template: path.join(__dirname,'src','index.html')
    }),
    new CopyPlugin([{
      from: path.resolve(__dirname,'src/data'), to: path.resolve(__dirname,'dist/data')
    }, 
    {
      from: path.resolve(__dirname,'src/img'), to: path.resolve(__dirname,'dist/img')
    }])
  ]
};

